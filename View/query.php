<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <title>Query</title>
</head>

<body>
  <h1 class="text-secondary border text-center">Query</h1>
  <nav class="nav">
    <a class="nav-link" href="profile.php">Profile</a>
    <a class="nav-link" href="init.php">Init</a>
    <a class="nav-link" href="transfer.php">Transfer</a>
    <a class="nav-link" href="logout.php">Logout</a>
  </nav>
  <form action="../Controller/controller.php" method="post">
    <div class="form-group col">
      <label for="pass">Transacciones:</label>
      <ul class="list-group">
        <li class="list-group-item">Transacción</li>
        <li class="list-group-item">Transacción</li>
        <li class="list-group-item">Transacción</li>
        <li class="list-group-item">Transacción</li>
      </ul>
    </div>
    <select class="custom-select" name="filterBankingTransaction">
      <option selected>Elige tu opción</option>
      <option value="1">Todas</option>
      <option value="2">Recibidas</option>
      <option value="3">Enviadas</option>
    </select>
  </form>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>